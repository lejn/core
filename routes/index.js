const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
  res.json({ status: 'Core is awesome!' });
});

router.post('/submit', (req, res, next) => {
  const submitRequest = req.body;

  // Call 3rd parties

  // Decision: APPROVE or DECLINE

  // if OK, ask for PIN

  res.json({ error: 'This is not a website' });
});

router.post('/pin', (req, res, next) => {
  const pinRequest = req.body;

  // send email/notification

  // tell Overlord to create a new thing with pin included

  res.json({ error: 'This is not a website' });
});

module.exports = router;
